var express = require('express');
var router = express.Router();
var usuarioController = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioController.usuarios_list);
router.post('/create', usuarioController.usuarios_create);
// router.delete('/delete', usuariosController.usuarios_delete);


module.exports = router;
// var express = require('express');
// var router = express.Router();
// var usuariosController = require('../controllers/usuarios.js');


// router.get('/', usuariosController.list);
// router.get('/create', usuariosController.create_get);
// router.post('/create', usuariosController.create);
// router.get('/:id/update', usuariosController.update_get);
// router.post('/:id/update', usuariosController.update);
// router.post('/:id/delete', usuariosController.delete);
// router.get('/login', usuariosController.login_get);
// router.post('/login', usuariosController.login_post);


// module.exports = router;